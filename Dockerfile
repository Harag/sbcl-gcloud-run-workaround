# Chose this ubuntu base image because its small and optimized for Docker(ness)
FROM    phusion/baseimage

# Installing what I need from ubuntu to do the job.
# - wget to download stuff from the web
#     -- curl gave me a 301 trying to download build app so I swiched to wget
# - sbcl and build-essentials - To build the version of sbcl downloaded
#     -- the sbcl in ubuntu is usually a bit dated that why we download what we want
# - libev-dev - is used by the woo http server that we are using for our example
#
RUN apt update &&\
    apt-get install -y sbcl wget build-essential time libev-dev

# Downloading, compiling and installing our prefered version of SBCL
RUN cd /tmp && \
    wget https://ufpr.dl.sourceforge.net/project/sbcl/sbcl/2.0.5/sbcl-2.0.5-source.tar.bz2 && \
    tar jxvf sbcl-2.0.5-source.tar.bz2 && \
    cd /tmp/sbcl-2.0.5 && \
    sh ./make.sh  && \
    sh ./install.sh && \
    rm -rf /tmp/sbcl*

# Downloading buildapp to build the app we want to run in this docker
# -- the version is hardcoded ... not ideal
RUN cd /tmp && \
    wget http://www.xach.com/lisp/buildapp.tgz && \
    tar xvf buildapp.tgz && \
    cd /tmp/buildapp-1.5.6 && \
    make install  && \
    rm -rf /tmp/buildapp*
 
# install quicklisp
RUN curl -k -o /tmp/quicklisp.lisp 'https://beta.quicklisp.org/quicklisp.lisp' && \
    sbcl --noinform --non-interactive --load /tmp/quicklisp.lisp --eval \
        '(quicklisp-quickstart:install :path "~/quicklisp/")' && \
    echo '#+quicklisp(push "/src" ql:*local-project-directories*)' >> ~/.sbclrc && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Creating a directory in the Docker image that will host our source code to load when running
RUN mkdir /src/

# Copying our sourcecode from the local machine to the docker image.
# -- you could use some git repository instead or quicklisp
COPY woo.lisp /src/woo.lisp

# Loading sbcl with all then packages woo needs and exporting the asd manifest to use
# when using buildapp
RUN sbcl --no-userinit \
         --no-sysinit --non-interactive \
	 --load ~/quicklisp/setup.lisp \
         --eval '(ql:quickload "woo")' \
         --eval '(ql:write-asdf-manifest-file "quicklisp-manifest.txt")'

# Building the app with my source code
RUN buildapp \
    --manifest-file quicklisp-manifest.txt \
    --load-system woo \
    --load /src/woo.lisp \
    --entry main \
    --output woo

# Woo uses port 5000 by default so exposing it to the world
EXPOSE 5000

# When then docker is run this is called to load our toy app.
CMD sleep 0.05; ./woo

