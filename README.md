# sbcl-gcloud-run-workaround

Today (14 Jun 2020) google-cloud-run has a different $HOME for RUN and CMD when executed in the Dockerfile which causes Quicklisp issues if you just want to use SBCL in the CMD.

This repo shows how to work around the issue using buildapp.

The jist of it is that we set up a sbcl with all our dependencies and then dump that in a core with buildapp tooling.

Buildapp produces something that CMD can call and has already got everything in our lisp up and running which avoids having to rely on $HOME to load stuff from quicklisp when we run our docker.

This would be what you would do to for production docker to start up fast, ie the less code you load in CMD when calling SBCL the better.

See heavily commented Dockerfile for more info.

## Testing locally

To build cd to the source directory

```sudo docker build --tag gcloud-sbcl:1.0 .```

To run

```sudo docker run -ti --publish 5000:5000  --name gcloud-sbcl gcloud-sbcl:1.0```

Now you can go to http://localhost:5000 to check out what woo is up to.

To git rid of then running docker

```sudo docker rm --force gcloud-sbcl.```

Now to get it on to gcloud cloudbuild.json has some settings change to your liking.

## gcloud build and deploy

In terminal

```gcloud builds submit --config cloudbuild.json```

```gcloud run deploy woo --image=eu.gcr.io/[project id]/gcloud-sbcl --platform managed --port 5000```

When prompted allow unauthenticated else you will have to jump through more loops to test it.

Wait for the deploy to give you a URL and then try the app with that url.
